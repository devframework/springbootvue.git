package org.dev.util.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.util.entity.SysSequenceQueue;

/**
 * <p>
 * 生成的队列号 Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-20
 */
public interface SysSequenceQueueMapper extends BaseMapper<SysSequenceQueue> {

}

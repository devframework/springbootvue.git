package org.dev.common.contant;

public interface UserType {
    String Administrator = "admin";
    String NormalUser = "user";
}

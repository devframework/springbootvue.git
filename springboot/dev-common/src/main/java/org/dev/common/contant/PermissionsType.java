package org.dev.common.contant;

public interface PermissionsType {

    /**
     * 菜单类型
     */
    String menu = "menu";
    /**
     * 方法类型
     */
    String func = "func";
}

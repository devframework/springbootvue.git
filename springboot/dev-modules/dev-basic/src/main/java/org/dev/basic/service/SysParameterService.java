package org.dev.basic.service;

import org.dev.basic.entity.SysParameter;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统参数 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2021-05-19
 */
public interface SysParameterService extends IService<SysParameter> {

}

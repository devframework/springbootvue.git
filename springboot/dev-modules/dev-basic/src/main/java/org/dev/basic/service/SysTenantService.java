package org.dev.basic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.basic.entity.SysTenant;

/**
 * <p>
 * 租户信息 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-05
 */
public interface SysTenantService extends IService<SysTenant> {

}

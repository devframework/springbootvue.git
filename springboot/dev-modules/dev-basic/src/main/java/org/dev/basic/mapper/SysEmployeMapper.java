package org.dev.basic.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.basic.entity.SysEmploye;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-08-02
 */
public interface SysEmployeMapper extends BaseMapper<SysEmploye> {

}

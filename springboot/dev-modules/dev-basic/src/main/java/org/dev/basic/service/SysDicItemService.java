package org.dev.basic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.basic.entity.SysDicItem;

/**
 * <p>
 * 数据库字典组-子级 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-05
 */
public interface SysDicItemService extends IService<SysDicItem> {

}

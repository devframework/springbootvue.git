package org.dev.basic.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.basic.entity.SysJobs;

/**
 * <p>
 * 岗位管理 Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-10-22
 */
public interface SysJobsMapper extends BaseMapper<SysJobs> {

}

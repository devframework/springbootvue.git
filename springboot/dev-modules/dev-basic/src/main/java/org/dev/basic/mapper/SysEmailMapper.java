package org.dev.basic.mapper;

import org.dev.basic.entity.SysEmail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2022-01-27
 */
public interface SysEmailMapper extends BaseMapper<SysEmail> {

}

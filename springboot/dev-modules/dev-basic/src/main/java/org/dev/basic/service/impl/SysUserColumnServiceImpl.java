package org.dev.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.dev.basic.entity.SysUserColumn;
import org.dev.basic.mapper.SysUserColumnMapper;
import org.dev.basic.service.SysUserColumnService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
@Service
public class SysUserColumnServiceImpl extends ServiceImpl<SysUserColumnMapper, SysUserColumn> implements SysUserColumnService {

}

package org.dev.basic.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dev.basic.entity.SysUpload;
import org.dev.basic.mapper.SysUploadMapper;
import org.dev.basic.service.SysUploadService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上传文件 服务实现类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-18
 */
@Service
public class SysUploadServiceImpl extends ServiceImpl<SysUploadMapper, SysUpload> implements SysUploadService {

}

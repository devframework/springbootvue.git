package org.dev.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.basic.entity.SysEmploye;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-08-02
 */
public interface SysEmployeService extends IService<SysEmploye> {

}

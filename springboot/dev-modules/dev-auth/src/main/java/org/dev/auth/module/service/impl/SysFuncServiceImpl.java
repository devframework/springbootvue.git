package org.dev.auth.module.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dev.auth.module.entity.SysFunc;
import org.dev.auth.module.mapper.SysFuncMapper;
import org.dev.auth.module.service.SysFuncService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
@Service
public class SysFuncServiceImpl extends ServiceImpl<SysFuncMapper, SysFunc> implements SysFuncService {

}

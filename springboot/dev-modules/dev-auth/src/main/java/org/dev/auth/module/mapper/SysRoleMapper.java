package org.dev.auth.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.auth.module.entity.SysRole;


/**
 * <p>
 * 权限管理-角色管理 Mapper 接口
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}

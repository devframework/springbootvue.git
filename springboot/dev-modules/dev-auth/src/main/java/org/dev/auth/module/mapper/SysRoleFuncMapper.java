package org.dev.auth.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.auth.module.entity.SysRoleFunc;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysRoleFuncMapper extends BaseMapper<SysRoleFunc> {

}

package org.dev.auth.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.auth.module.entity.SysFunc;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysFuncMapper extends BaseMapper<SysFunc> {

}

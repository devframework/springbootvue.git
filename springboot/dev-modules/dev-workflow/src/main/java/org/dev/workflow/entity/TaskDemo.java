package org.dev.workflow.entity;

import lombok.Data;

@Data
public class TaskDemo {
    private String code;

    private String name;

    private double total;

}

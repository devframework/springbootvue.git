package org.dev.workflow.util;

public interface AttributeName {
    /**
     * 签核节点所选值
     */
    String AssigneeSelect = "assigneeSelect";

    /**
     * 签核节点签核类型
     */
    String AssigneeType = "assigneeType";
}
